package main

import (
	"time"
	"fmt"
	"net"
	"strconv"
	"gitlab.com/gamerscomplete/robotics/hexapod/hexnet"
	"encoding/gob"
	"flag"
)

var (
    port = flag.Int("port", 42069, "server port to connect to")
    addr = flag.String("host", "", "server host to connect to")
)

func main() {
	flag.Parse()

	if *addr == "" {
		fmt.Println("addr is a required flag")
		return
	}

    conn, err := net.DialTimeout("tcp", *addr+":"+strconv.Itoa(*port), time.Second*1)
    if err != nil {
		fmt.Println(err)
		return
    }
    decoder := gob.NewDecoder(conn)
    encoder := gob.NewEncoder(conn)

	start := time.Now()

	message := hexnet.Message(hexnet.Ping{Val: "test"})

	err = encoder.Encode(&message)
	if err != nil {
		fmt.Println("Failed to send message: ", err)
		return
	}

	fmt.Println("message send time: ", time.Since(start))
	var reply string
	err = decoder.Decode(&reply)
	if err != nil {
		fmt.Println("failed to decode reply: ", err)
		return
	}
	fmt.Println("ReplY: ", reply)
	duration := time.Since(start)
	fmt.Println("ping response time ", duration)
}
