package main

import (
	"time"
	"flag"
	"io/ioutil"
	"fmt"
	"github.com/jacobsa/go-serial/serial"
	"gitlab.com/gamerscomplete/robotics/lx16a"
	log "github.com/sirupsen/logrus"
)

var (
	serialPort     = flag.String("serial-port", "/dev/ttyUSB0", "path to the serial port")
)

func main() {
	flag.Parse()

	sOpts := serial.OpenOptions{
		PortName:              *serialPort,
		BaudRate:              115200,
		DataBits:              8,
		StopBits:              1,
		MinimumReadSize:       0,
		InterCharacterTimeout: 100,
	}


	fmt.Println("opening serial")
	srl, err := serial.Open(sOpts)
	if err != nil {
		log.Fatalf("error opening serial port: %s\n", err)
	}
	defer srl.Close()
	var b []byte
	log.Info("purging serial buffer")
	b, err = ioutil.ReadAll(srl)
	if err != nil {
		log.Fatalf("error purging serial buffer: %s\n", err)
	}
	log.Infof("purged %d bytes", len(b))

	fmt.Println("new network")
	network := lx16a.NewNetwork(srl)
	fmt.Println("creating servo 2")
	servo2 := lx16a.NewServo(network, 2)
	for {
		servo2.TimedMove(0, 0)
		time.Sleep(2 * time.Second)
		servo2.TimedMove(180, 0)
		time.Sleep(2 * time.Second)
	}

/*
	fmt.Println("getting servo id")
	id, err := servo1.GetID()
	if err != nil {
		fmt.Println("failed to get servo id:", err)
		return
	}
	fmt.Println("servo id:", id)

	fmt.Println("flushing serial")
	network.Flush()

	fmt.Println("Setting servo id")
	if err = servo1.SetID(2); err != nil {
		fmt.Println("failed to set servo id:", err)
		return
	}
	fmt.Println("successfully set servo id")

	id, err = servo1.GetID()
	if err != nil {
		fmt.Println("failed to get servo id:", err)
		return
	}
	fmt.Println("servo id:", id)
*/

}
