/*
TODO:
	tools
		move servo (id, angle, time)
		implement all servo functions as pass through from rpc
	monitor servos for data and save it
	implement fetcher for stats that doesnt require polling them all individually, use monitored stats
	setup triggers on stats
		over voltage
		over temp
		stalled
	emergency off

Longterm:
	sensors:
		sonar
		lidar
		led control
		camera
		camera gimbal control
		current monitoring on servo feed
		voltage monitoring on battery side
		kill button
		six axis control
*/

package main

import (
	"errors"
	"flag"
	"fmt"
	"github.com/jacobsa/go-serial/serial"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gamerscomplete/robotics/hexapod/hexnet"
	"gitlab.com/gamerscomplete/robotics/lx16a"
	"io"
	"io/ioutil"
	"time"
)

var (
	serialPort  = flag.String("serial-port", "/dev/ttyUSB0", "path to the serial port")
	coxaAngle   = flag.Float64("coxa", 90, "coxa angles")
	femurAngle  = flag.Float64("femur", 90, "femur angles")
	tibiaAngle  = flag.Float64("tibia", 90, "tibia angles")
	angleTime   = flag.Int("angle-time", 0, "milliseconds to make the move")
	serverPort  = flag.Int("server-port", 42069, "port to listen for communication on")
	monitorRate = flag.Int("monitor-rate", 60, "number of times to poll the servos per minute")
)

type Hexapod struct {
	tibia          [6]*lx16a.Servo
	coxa           [6]*lx16a.Servo
	femur          [6]*lx16a.Servo
	servos         [18]*lx16a.Servo
	stopMonitor    bool
	monitorRate    int
	monitorRunning bool
	servoStats     [18]hexnet.Stats
}

func main() {
	flag.Parse()

	sOpts := serial.OpenOptions{
		PortName:              *serialPort,
		BaudRate:              115200,
		DataBits:              8,
		StopBits:              1,
		MinimumReadSize:       0,
		InterCharacterTimeout: 100,
	}

	fmt.Println("opening serial")
	srl, err := serial.Open(sOpts)
	if err != nil {
		log.Fatalf("error opening serial port: %s\n", err)
	}
	defer srl.Close()
	var b []byte
	log.Info("purging serial buffer")
	b, err = ioutil.ReadAll(srl)
	if err != nil {
		log.Fatalf("error purging serial buffer: %s\n", err)
	}
	log.Infof("purged %d bytes", len(b))

	hexapod := NewHexapod(srl)
	_ = hexnet.NewServer(*serverPort, hexapod.MessageHandler)

	go hexapod.monitorServos()
	/*
		//TEMP
		err = hexapod.SetUniformAngles(90, 135, 135, 0)
		if err != nil {
			fmt.Println("failed to set uniform angles: ", err)
			return
		}
	*/
}

func NewHexapod(srl io.ReadWriteCloser) *Hexapod {
	hexapod := Hexapod{
		//		Servos: make([18]*lx16a.Servo),
		//		Coxa: make([6]*lx16a.Servo),
		//		Femur: make([6]*lx16a.Servo),
		//		Tibia: make([6]*lx16a.Servo),
	}
	network := lx16a.NewNetwork(srl)

	servo := uint8(1)
	for leg := 0; leg < 6; leg++ {
		hexapod.coxa[leg] = lx16a.NewServo(network, servo)
		hexapod.servos[servo-1] = hexapod.coxa[leg]
		servo++
		hexapod.femur[leg] = lx16a.NewServo(network, servo)
		hexapod.servos[servo-1] = hexapod.femur[leg]
		servo++
		hexapod.tibia[leg] = lx16a.NewServo(network, servo)
		hexapod.servos[servo-1] = hexapod.tibia[leg]
		servo++
	}
	/*
		hexapod := Hexapod{
			Coxa: [6]*lx16a.Servo{
				lx16a.NewServo(network, 1),
				lx16a.NewServo(network, 4),
				lx16a.NewServo(network, 7),
				lx16a.NewServo(network, 10),
				lx16a.NewServo(network, 13),
				lx16a.NewServo(network, 16),
			},
			Femur: [6]*lx16a.Servo{
				lx16a.NewServo(network, 2),
				lx16a.NewServo(network, 5),
				lx16a.NewServo(network, 8),
				lx16a.NewServo(network, 11),
				lx16a.NewServo(network, 14),
				lx16a.NewServo(network, 17),
			},
			Tibia: [6]*lx16a.Servo{
				lx16a.NewServo(network, 3),
				lx16a.NewServo(network, 6),
				lx16a.NewServo(network, 9),
				lx16a.NewServo(network, 12),
				lx16a.NewServo(network, 15),
				lx16a.NewServo(network, 18),
			},
		}
	*/
	return &hexapod
}

func (hexa *Hexapod) SetUniformAngles(coxa float32, femur float32, tibia float32, duration uint16) error {
	for key, _ := range hexa.coxa {
		err := hexa.coxa[key].TimedMove(coxa, duration)
		if err != nil {
			return err
		}
	}
	for key, _ := range hexa.femur {
		err := hexa.femur[key].TimedMove(femur, duration)
		if err != nil {
			return err
		}
	}
	for key, _ := range hexa.tibia {
		err := hexa.tibia[key].TimedMove(tibia, duration)
		if err != nil {
			return err
		}
	}
	return nil
}

func (hexa *Hexapod) GetAllStats() ([]hexnet.Stats, error) {
	stats := make([]hexnet.Stats, 0)
	//	stats := make([18]Stats)
	for i := 0; i < 18; {
		stat, err := hexa.GetStats(uint8(i + 1))
		if err != nil {
			return stats, fmt.Errorf("Failed to get stats for servo %d: %v", i+1, err)
		}
		stats = append(stats, stat)
	}
	return stats, nil

}

func (hexa *Hexapod) GetStats(servo uint8) (hexnet.Stats, error) {
	var stats hexnet.Stats
	voltage, err := hexa.servos[int(servo)].GetVoltage()
	if err != nil {
		return stats, err
	}
	stats.Voltage = uint16(voltage)

	temp, err := hexa.servos[int(servo)].GetTemp()
	if err != nil {
		return stats, err
	}
	stats.Temp = uint16(temp)

	voltage, err = hexa.servos[int(servo)].GetAngle()
	if err != nil {
		return stats, err
	}

	return stats, nil

}

func (hexa *Hexapod) MessageHandler(message hexnet.Message, client *hexnet.Client) error {
	//	start := time.Now()
	switch v := message.(type) {
	case *hexnet.Ping:
		//		fmt.Println("assertion time: ", time.Since(start))
		//		reply := hexnet.Message(&hexnet.Ping{Val: "pong"})
		//		encodeStart := time.Now()
		if err := client.Encoder.Encode("pong"); err != nil {
			return err
		}
		//		fmt.Println("encode time: ", time.Since(encodeStart))
		//		fmt.Println("printing time ", time.Since(encodeStart))
	case *hexnet.StatsRequest:
		fmt.Println("got stats request")
		if v.ServoID > 18 && v.ServoID != 255 {
			return errors.New("servo not found")
		}
		//TODO: get stats, return stats
		var stats []hexnet.Stats
		var err error
		if v.ServoID == 255 {
			stats[0], err = hexa.GetStats(v.ServoID)
		} else {
			stats, err = hexa.GetAllStats()
		}
		if err != nil {
			return err
		}
		if err := client.Encoder.Encode(stats); err != nil {
			return err
		}
	default:
		fmt.Printf("unknown message type %T\n", v)
		return errors.New("unknown message type")
	}
	//	fmt.Println("total hex message handling time: ", time.Since(start))
	return nil
}

func (hexa *Hexapod) monitorServos() {
	//We dont want 2 monitors running at the same time
	if hexa.monitorRunning {
		return
	}
	hexa.monitorRunning = true
	var timer time.Time
	var duration time.Duration
	var lastUpdate time.Time
	for !hexa.stopMonitor {
		timer = time.Now()
		stats := hexa.GetAllStats()
		for k, _ := range stats {
			hexa.servoStats[k] = stats[k]
		}
		duration = time.Since(timer)

	}
	hexa.monitorRunning = false
}

func (hexa *Hexapod) calibrationMode() {
	//turn off motors
	//prompt for each leg
	//prompt for each joint of the leg for min range, max range
	//save all the offsets for use setting angles

}
