package hexnet

import (
	"encoding/gob"
//	"time"
	"fmt"
	"io"
	"net"
	"strconv"
	"sync"
)

type Handler func(Message,*Client)error

type Server struct {
	clients    []*Client
	clientLock sync.RWMutex
	handler Handler
}

type Client struct {
	conn    net.Conn
}

func NewServer(port int, messageHandler Handler) *Server {
	server := Server{
		clients: make([]*Client, 0),
		handler: messageHandler,
	}

	l, err := net.Listen("udp", ":"+strconv.Itoa(port))
	if err != nil {
		fmt.Println("listen error: ", err)
	}

	for {
		conn, err := l.Accept()
		if err != nil {
			fmt.Println(4, "accept error:", err)
		}
		client := server.addClient(conn)
		go server.handleClient(client)
	}
}

func (server *Server) addClient(conn net.Conn) *Client {
	client := &Client{
		conn:    conn,
		Encoder: gob.NewEncoder(conn),
		decoder: gob.NewDecoder(conn),
	}

	server.clientLock.Lock()
	server.clients = append(server.clients, client)
	server.clientLock.Unlock()
	return client
}

func (server *Server) handleClient(client *Client) {
	for {
//		start := time.Now()
		var message Message
		err := client.decoder.Decode(&message)
//		fmt.Println("decode time: ", time.Since(start))
		if err == io.EOF {
//			fmt.Println("client hungup")
			server.clientLock.Lock()
			for k, v := range server.clients {
				if v == client {
					server.clients[k] = server.clients[len(server.clients)-1]
					server.clients = server.clients[:len(server.clients)-1]
					break
				}
			}
			server.clientLock.Unlock()
			return
		} else if err != nil {
			fmt.Println("failed to decode message: ", err)
			return
		}

		if message == nil {
			fmt.Println("message came through nil")
			continue
		}

		server.handler(message, client)
//		fmt.Println("total handling time: ", time.Since(start))
	}
}
