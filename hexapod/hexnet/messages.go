package hexnet

import (
	"encoding/gob"
)

type Command int

type Message interface{}

type Ping struct {
	Val string
}

type StatsRequest struct {
	ServoID uint8
}

type Stats struct {
    Temp    uint16
    Voltage uint16
    Angle   float32
}

type Error struct {
	Message string
}

func init() {
	gob.Register(&Ping{})
	gob.Register(&StatsRequest{})
	gob.Register(&Stats{})
	gob.Register(&Error{})
}
