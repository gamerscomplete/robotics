package main

import (
	"math"
	"fmt"
)

type Kinematic struct {
	BodySideLength float64
	CoxaLength     float64
	FemurLength    float64
	TibiaLength    float64
}

func NewKinematic(bodySideLength, coxaLength, femurLength, tibiaLength float64) *Kinematic {
	kinematic := &Kinematic{
		BodySideLength: bodySideLength,
		CoxaLength:     coxaLength,
		FemurLength:    femurLength,
		TibiaLength:    tibiaLength,
	}
	return kinematic
}

func (kinematic *Kinematic) calculateServoAngles(posX, posY, posZ, rotX, rotY, rotZ float64) []float64 {
	//Coxa servos
	//Body center offset x
	BodyCenterOffset1 := kinematic.BodySideLength / 2
	BodyCenterOffset2 := math.Sqrt(math.Pow(kinematic.BodySideLength,2) - math.Pow(BodyCenterOffset1,2))

	BodyCenterOffsetX1 := BodyCenterOffset1
	BodyCenterOffsetX2 := kinematic.BodySideLength
	BodyCenterOffsetX3 := BodyCenterOffset1
	BodyCenterOffsetX4 := -BodyCenterOffset1
	BodyCenterOffsetX5 := -kinematic.BodySideLength
	BodyCenterOffsetX6 := -BodyCenterOffset1

	//body center offset y
	BodyCenterOffsetY1 := BodyCenterOffset2
	BodyCenterOffsetY2 := float64(0)
	BodyCenterOffsetY3 := -BodyCenterOffset2
	BodyCenterOffsetY4 := -BodyCenterOffset2
	BodyCenterOffsetY5 := float64(0)
	BodyCenterOffsetY6 := BodyCenterOffset2

	//feet positions
	//leg 1
	FeetPosX1 := math.Cos(60/180*math.Pi) * (kinematic.CoxaLength + kinematic.FemurLength)
	FeetPosZ1 := kinematic.TibiaLength
	FeetPosY1 := math.Sin(60/180*math.Pi) * (kinematic.CoxaLength + kinematic.FemurLength)

	//leg 2
	FeetPosX2 := kinematic.CoxaLength + kinematic.FemurLength
	FeetPosZ2 := kinematic.TibiaLength
	FeetPosY2 := float64(0)

	//leg 3
	FeetPosX3 := math.Cos(60/180*math.Pi) * (kinematic.CoxaLength + kinematic.FemurLength)
	FeetPosZ3 := kinematic.TibiaLength
	FeetPosY3 := math.Sin(-60/180*math.Pi) * (kinematic.CoxaLength + kinematic.FemurLength)

	//leg 4
	FeetPosX4 := -math.Cos(60/180*math.Pi) * (kinematic.CoxaLength + kinematic.FemurLength)
	FeetPosZ4 := kinematic.TibiaLength
	FeetPosY4 := math.Sin(-60/180*math.Pi) * (kinematic.CoxaLength + kinematic.FemurLength)

	//leg 5
	FeetPosX5 := -(kinematic.CoxaLength + kinematic.FemurLength)
	FeetPosZ5 := kinematic.TibiaLength
	FeetPosY5 := float64(0)

	//leg 6
	FeetPosX6 := -math.Cos(60/180*math.Pi) * (kinematic.CoxaLength + kinematic.FemurLength)
	FeetPosZ6 := kinematic.TibiaLength
	FeetPosY6 := math.Sin(60/180*math.Pi) * (kinematic.CoxaLength + kinematic.FemurLength)

	//body inverse kinematics
	//leg 1
	TotalY1 := FeetPosY1 + BodyCenterOffsetY1 + posY
	TotalX1 := FeetPosX1 + BodyCenterOffsetX1 + posX
	DistBodyCenterFeet1 := math.Sqrt(TotalY1*TotalY1 + TotalX1*TotalX1)
	AngleBodyCenterX1 := math.Pi/2 - math.Atan2(TotalY1, TotalX1)
	RollZ1 := math.Tan(rotZ*math.Pi/180) * TotalX1
	PitchZ1 := math.Tan(rotX*math.Pi/180) * TotalY1
	BodyIKX1 := math.Cos(AngleBodyCenterX1+(rotY*math.Pi/180))*DistBodyCenterFeet1 - TotalX1
	BodyIKY1 := (math.Sin(AngleBodyCenterX1+(rotY*math.Pi/180)) * DistBodyCenterFeet1) - TotalY1
	BodyIKZ1 := RollZ1 + PitchZ1

	//leg 2
	TotalY2 := FeetPosY2 + BodyCenterOffsetY2 + posY
	TotalX2 := FeetPosX2 + posX + BodyCenterOffsetX2
	DistBodyCenterFeet2 := math.Sqrt(math.Pow(TotalY2,2) + math.Pow(TotalX2,2))
	AngleBodyCenterX2 := math.Pi/2 - math.Atan2(TotalY2, TotalX2)
	RollZ2 := math.Tan(rotZ*math.Pi/180) * TotalX2
	PitchZ2 := math.Tan(rotX*math.Pi/180) * TotalY2
	BodyIKX2 := math.Cos(AngleBodyCenterX2+(rotY*math.Pi/180))*DistBodyCenterFeet2 - TotalX2
	BodyIKY2 := (math.Sin(AngleBodyCenterX2+(rotY*math.Pi/180)) * DistBodyCenterFeet2) - TotalY2
	BodyIKZ2 := RollZ2 + PitchZ2

	//leg 3
	TotalY3 := FeetPosY3 + BodyCenterOffsetY3 + posY
	TotalX3 := FeetPosX3 + BodyCenterOffsetX3 + posX
	DistBodyCenterFeet3 := math.Sqrt(TotalY3*TotalY3 + TotalX3*TotalX3)
	AngleBodyCenterX3 := math.Pi/2 - math.Atan2(TotalY3, TotalX3)
	RollZ3 := math.Tan(rotZ*math.Pi/180) * TotalX3
	PitchZ3 := math.Tan(rotX*math.Pi/180) * TotalY3
	BodyIKX3 := math.Cos(AngleBodyCenterX3+(rotY*math.Pi/180))*DistBodyCenterFeet3 - TotalX3
	BodyIKY3 := (math.Sin(AngleBodyCenterX3+(rotY*math.Pi/180)) * DistBodyCenterFeet3) - TotalY3
	BodyIKZ3 := RollZ3 + PitchZ3

	//leg 4
	TotalY4 := FeetPosY4 + BodyCenterOffsetY4 + posY
	TotalX4 := FeetPosX4 + BodyCenterOffsetX4 + posX
	DistBodyCenterFeet4 := math.Sqrt(TotalY4*TotalY4 + TotalX4*TotalX4)
	AngleBodyCenterX4 := math.Pi/2 - math.Atan2(TotalY4, TotalX4)
	RollZ4 := math.Tan(rotZ*math.Pi/180) * TotalX4
	PitchZ4 := math.Tan(rotX*math.Pi/180) * TotalY4
	BodyIKX4 := math.Cos(AngleBodyCenterX4+(rotY*math.Pi/180))*DistBodyCenterFeet4 - TotalX4
	BodyIKY4 := (math.Sin(AngleBodyCenterX4+(rotY*math.Pi/180)) * DistBodyCenterFeet4) - TotalY4
	BodyIKZ4 := RollZ4 + PitchZ4

	//leg 5
	TotalY5 := FeetPosY5 + BodyCenterOffsetY5 + posY
	TotalX5 := FeetPosX5 + BodyCenterOffsetX5 + posX
	DistBodyCenterFeet5 := math.Sqrt(TotalY5*TotalY5 + TotalX5*TotalX5)
	AngleBodyCenterX5 := math.Pi/2 - math.Atan2(TotalY5, TotalX5)
	RollZ5 := math.Tan(rotZ*math.Pi/180) * TotalX5
	PitchZ5 := math.Tan(rotX*math.Pi/180) * TotalY5
	BodyIKX5 := math.Cos(AngleBodyCenterX5+(rotY*math.Pi/180))*DistBodyCenterFeet5 - TotalX5
	BodyIKY5 := (math.Sin(AngleBodyCenterX5+(rotY*math.Pi/180)) * DistBodyCenterFeet5) - TotalY5
	BodyIKZ5 := RollZ5 + PitchZ5

	//leg 6
	TotalY6 := FeetPosY6 + BodyCenterOffsetY6 + posY
	TotalX6 := FeetPosX6 + BodyCenterOffsetX6 + posX
	DistBodyCenterFeet6 := math.Sqrt(TotalY6*TotalY6 + TotalX6*TotalX6)
	AngleBodyCenterX6 := math.Pi/2 - math.Atan2(TotalY6, TotalX6)
	RollZ6 := math.Tan(rotZ*math.Pi/180) * TotalX6
	PitchZ6 := math.Tan(rotX*math.Pi/180) * TotalY6
	BodyIKX6 := math.Cos(AngleBodyCenterX6+(rotY*math.Pi/180))*DistBodyCenterFeet6 - TotalX6
	BodyIKY6 := (math.Sin(AngleBodyCenterX6+(rotY*math.Pi/180)) * DistBodyCenterFeet6) - TotalY6
	BodyIKZ6 := RollZ6 + PitchZ6

	//Leg inverse kinematics
	//leg 1
	NewPosX1 := FeetPosX1 + posX + BodyIKX1
	NewPosZ1 := FeetPosZ1 + posZ + BodyIKZ1
	NewPosY1 := FeetPosY1 + posY + BodyIKY1
	CoxaFeetDist1 := math.Sqrt(math.Pow(NewPosX1,2) + math.Pow(NewPosY1,2))
	IKSW1 := math.Sqrt(math.Pow(CoxaFeetDist1-kinematic.CoxaLength,2) + math.Pow(NewPosZ1,2))
	IKA11 := math.Atan((CoxaFeetDist1 - kinematic.CoxaLength) / NewPosZ1)
	IKA21 := math.Acos((math.Pow(kinematic.TibiaLength,2) - math.Pow(kinematic.FemurLength,2) - math.Pow(IKSW1,2)) / (-2 * IKSW1 * kinematic.FemurLength))
	TAngle1 := math.Acos((math.Pow(IKSW1,2) - math.Pow(kinematic.TibiaLength,2) - math.Pow(kinematic.FemurLength,2)) / (-2 * kinematic.FemurLength * kinematic.TibiaLength))
	IKTibiaAngle1 := 90 - TAngle1*180/math.Pi
	IKFemurAngle1 := 90 - (IKA11+IKA21)*180/math.Pi
	IKCoxaAngle1 := 90 - math.Atan2(NewPosY1, NewPosX1)*180/math.Pi

	//leg 2
	NewPosX2 := FeetPosX2 + posX + BodyIKX2
	NewPosZ2 := FeetPosZ2 + posZ + BodyIKZ2
	NewPosY2 := FeetPosY2 + posY + BodyIKY2
	CoxaFeetDist2 := math.Sqrt(math.Pow(NewPosX2,2) + math.Pow(NewPosY2,2))

	IKSW2 := math.Sqrt(math.Pow(CoxaFeetDist2-kinematic.CoxaLength, 2) + math.Pow(NewPosZ2, 2))
	fmt.Printf("IKSW: %f -+- math.Sqrt(math.Pow(%f, 2) + math.Pow(%f, 2))\n",
		IKSW2,
		CoxaFeetDist2-kinematic.CoxaLength,
		NewPosZ2,
	)

	IKA12 := math.Atan((CoxaFeetDist2 - kinematic.CoxaLength) / NewPosZ2)
	IKA22 := math.Acos((math.Pow(kinematic.TibiaLength, 2) - math.Pow(kinematic.FemurLength, 2) - math.Pow(IKSW2, 2)) / (-2 * IKSW2 * kinematic.FemurLength))
	fmt.Printf("IKA22: %f -=- math.Acos((%f - %f - %f) / (-2 * %f * %f))\n",
		IKA22,
		math.Pow(kinematic.TibiaLength, 2),
		math.Pow(kinematic.FemurLength, 2),
		math.Pow(IKSW2, 2),
		IKSW2,
		kinematic.FemurLength,
	)
	TAngle2 := math.Acos((math.Pow(IKSW2, 2) - math.Pow(kinematic.TibiaLength, 2) - math.Pow(kinematic.FemurLength, 2)) / (-2 * kinematic.FemurLength * kinematic.TibiaLength))
	IKTibiaAngle2 := 90 - TAngle2*180/math.Pi
	IKFemurAngle2 := 90 - (IKA12+IKA22)*180/math.Pi
	fmt.Printf("femur angle 2: 90 - (%f+%f)*180/%f\n", IKA12, IKA22, math.Pi)
	IKCoxaAngle2 := 90 - math.Atan2(NewPosY2, NewPosX2)*180/math.Pi

	//leg 3
	NewPosX3 := FeetPosX3 + posX + BodyIKX3
	NewPosZ3 := FeetPosZ3 + posZ + BodyIKZ3
	//This seems to have been documented wrong because that var isnt used otherwise and all the others use the proper number for their leg
	//	NewPosY3 := FeetPosY2 + posY + BodyIKY2
	NewPosY3 := FeetPosY2 + posY + BodyIKY3
	CoxaFeetDist3 := math.Sqrt(math.Pow(NewPosX3, 2) + math.Pow(NewPosY3, 2))
	IKSW3 := math.Sqrt(math.Pow(CoxaFeetDist3-kinematic.CoxaLength, 2) + math.Pow(NewPosZ3, 2))
	IKA13 := math.Atan((CoxaFeetDist3 - kinematic.CoxaLength) / NewPosZ3)
	IKA23 := math.Acos((math.Pow(kinematic.TibiaLength, 2) - math.Pow(kinematic.FemurLength, 2) - math.Pow(IKSW3, 2)) / (-2 * IKSW3 * kinematic.FemurLength))
	TAngle3 := math.Acos((math.Pow(IKSW3, 2) - math.Pow(kinematic.TibiaLength, 2) - math.Pow(kinematic.FemurLength, 2)) / (-2 * kinematic.FemurLength * kinematic.TibiaLength))
	IKTibiaAngle3 := 90 - TAngle3*180/math.Pi
	IKFemurAngle3 := 90 - (IKA13+IKA23)*180/math.Pi
	IKCoxaAngle3 := 90 - math.Atan2(NewPosY3, NewPosX3)*180/math.Pi

	//leg 4
	NewPosX4 := FeetPosX4 + posX + BodyIKX4
	NewPosZ4 := FeetPosZ4 + posZ + BodyIKZ4
	NewPosY4 := FeetPosY4 + posY + BodyIKY4
	CoxaFeetDist4 := math.Sqrt(math.Pow(NewPosX4, 2) + math.Pow(NewPosY4, 2))
	IKSW4 := math.Sqrt(math.Pow(CoxaFeetDist4-kinematic.CoxaLength, 2) + math.Pow(NewPosZ4, 2))
	IKA14 := math.Atan((CoxaFeetDist4 - kinematic.CoxaLength) / NewPosZ4)
	IKA24 := math.Acos((math.Pow(kinematic.TibiaLength, 2) - math.Pow(kinematic.FemurLength, 2) - math.Pow(IKSW4, 2)) / (-2 * IKSW4 * kinematic.FemurLength))
	TAngle4 := math.Acos((math.Pow(IKSW4, 2) - math.Pow(kinematic.TibiaLength, 2) - math.Pow(kinematic.FemurLength, 2)) / (-2 * kinematic.FemurLength * kinematic.TibiaLength))
	IKTibiaAngle4 := 90 - TAngle4*180/math.Pi
	IKFemurAngle4 := 90 - (IKA14+IKA24)*180/math.Pi
	IKCoxaAngle4 := 90 - math.Atan2(NewPosY4, NewPosX4)*180/math.Pi

	//leg 5
	NewPosX5 := FeetPosX5 + posX + BodyIKX5
	NewPosZ5 := FeetPosZ5 + posX + BodyIKZ5
	NewPosY5 := FeetPosY5 + posY + BodyIKY5
	CoxaFeetDist5 := math.Sqrt(math.Pow(NewPosX5, 2) + math.Pow(NewPosY5, 2))
	IKSW5 := math.Sqrt(math.Pow(CoxaFeetDist5-kinematic.CoxaLength, 2) + math.Pow(NewPosZ5, 2))
	IKA15 := math.Atan((CoxaFeetDist5 - kinematic.CoxaLength) / NewPosZ5)
	IKA25 := math.Acos((math.Pow(kinematic.TibiaLength, 2) - math.Pow(kinematic.FemurLength, 2) - math.Pow(IKSW5, 2)) / (-2 * IKSW5 * kinematic.FemurLength))
	TAngle5 := math.Acos((math.Pow(IKSW5, 2) - math.Pow(kinematic.TibiaLength, 2) - math.Pow(kinematic.FemurLength, 2)) / (-2 * kinematic.FemurLength * kinematic.TibiaLength))
	IKTibiaAngle5 := 90 - TAngle5*180/math.Pi
	IKFemurAngle5 := 90 - (IKA15+IKA25)*180/math.Pi
	IKCoxaAngle5 := 90 - math.Atan2(NewPosY5, NewPosX5)*180/math.Pi

	//leg 6
	NewPosX6 := FeetPosX6 + posX + BodyIKX6
	NewPosZ6 := FeetPosZ6 + posZ + BodyIKZ6
	NewPosY6 := FeetPosY6 + posY + BodyIKY6
	CoxaFeetDist6 := math.Sqrt(math.Pow(NewPosX6, 2) + math.Pow(NewPosY6, 2))
	IKSW6 := math.Sqrt(math.Pow(CoxaFeetDist6-kinematic.CoxaLength, 2) + math.Pow(NewPosZ6, 2))
	IKA16 := math.Atan((CoxaFeetDist6 - kinematic.CoxaLength) / NewPosZ6)
	IKA26 := math.Acos((math.Pow(kinematic.TibiaLength, 2) - math.Pow(kinematic.FemurLength, 2) - math.Pow(IKSW6, 2)) / (-2 * IKSW6 * kinematic.FemurLength))
	TAngle6 := math.Acos((math.Pow(IKSW6, 2) - math.Pow(kinematic.TibiaLength, 2) - math.Pow(kinematic.FemurLength, 2)) / (-2 * kinematic.FemurLength * kinematic.TibiaLength))
	IKTibiaAngle6 := 90 - TAngle6*180/math.Pi
	IKFemurAngle6 := 90 - (IKA16+IKA26)*180/math.Pi
	IKCoxaAngle6 := 90 - math.Atan2(NewPosY6, NewPosX6)*180/math.Pi

	//Servo angles
	//Leg 1
	CoxaAngle1 := IKCoxaAngle1 - 60
	FemurAngle1 := IKFemurAngle1
	TibiaAngle1 := IKTibiaAngle1

	//leg 2
	CoxaAngle2 := IKCoxaAngle2
	FemurAngle2 := IKFemurAngle2
	TibiaAngle2 := IKTibiaAngle2

	//leg 3
	CoxaAngle3 := IKCoxaAngle3 + 60
	FemurAngle3 := IKFemurAngle3
	TibiaAngle3 := IKTibiaAngle3

	//leg 4
	CoxaAngle4 := IKCoxaAngle4 - 240
	FemurAngle4 := IKFemurAngle4
	TibiaAngle4 := IKTibiaAngle4

	//leg 5
	CoxaAngle5 := IKCoxaAngle5 - 180
	FemurAngle5 := IKFemurAngle5
	TibiaAngle5 := IKTibiaAngle5

	//leg 6
	CoxaAngle6 := IKCoxaAngle6 - 120
	FemurAngle6 := IKFemurAngle6
	TibiaAngle6 := IKTibiaAngle6

	return []float64{
		CoxaAngle1,
		FemurAngle1,
		TibiaAngle1,
		CoxaAngle2,
		FemurAngle2,
		TibiaAngle2,
		CoxaAngle3,
		FemurAngle3,
		TibiaAngle3,
		CoxaAngle4,
		FemurAngle4,
		TibiaAngle4,
		CoxaAngle5,
		FemurAngle5,
		TibiaAngle5,
		CoxaAngle6,
		FemurAngle6,
		TibiaAngle6,
	}
}
