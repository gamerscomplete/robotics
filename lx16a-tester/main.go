package main

import (
	"flag"
	"fmt"
	"github.com/jacobsa/go-serial/serial"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gamerscomplete/robotics/lx16a"
	"io/ioutil"
)

var (
	serialPort = flag.String("serial-port", "/dev/ttyUSB0", "path to the serial port")
	action = flag.String("action", "info", "what type of action to perform")
	servo = flag.Int("servo", 2, "what servo to talk to")
	angle = flag.Float64("angle", 0.0, "angle to set the servo to")
	angleTime = flag.Int("angle-time", 0, "milliseconds to make the move")
)

func main() {
	flag.Parse()

	sOpts := serial.OpenOptions{
		PortName:              *serialPort,
		BaudRate:              115200,
		DataBits:              8,
		StopBits:              1,
		MinimumReadSize:       0,
		InterCharacterTimeout: 100,
	}

	fmt.Println("opening serial")
	srl, err := serial.Open(sOpts)
	if err != nil {
		log.Fatalf("error opening serial port: %s\n", err)
	}
	defer srl.Close()
	var b []byte
	log.Info("purging serial buffer")
	b, err = ioutil.ReadAll(srl)
	if err != nil {
		log.Fatalf("error purging serial buffer: %s\n", err)
	}
	log.Infof("purged %d bytes", len(b))

	network := lx16a.NewNetwork(srl)
	thisServo := lx16a.NewServo(network, uint8(*servo))

	switch *action {
	case "pos":
		err := thisServo.TimedMove(float32(*angle), 0)
		if err != nil {
			fmt.Println("failed to do a time move: ", err)
			return
		}
	case "info":
		temp, err := thisServo.GetTemp()
		if err != nil {
			fmt.Println("Failed to fetch temp: ", err)
			return
		}
		fmt.Println("servo temp: ", temp, " converted temp: ", lx16a.CToF(temp))

		voltage, err := thisServo.GetVoltage()
		if err != nil {
			fmt.Println("failed to fetch voltage: ", err)
			return
		}
		fmt.Println("voltage: ", voltage)

		angle, err := thisServo.GetAngle()
		if err != nil {
			fmt.Println("failed to fetch angle: ", err)
			return
		}
		fmt.Println("angle: ", angle)
	default:
		fmt.Println("unknown action: ", action)
		return
	}
}
