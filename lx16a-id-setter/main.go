package main

import (
	"flag"
	"fmt"
	"github.com/jacobsa/go-serial/serial"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gamerscomplete/robotics/lx16a"
	"io/ioutil"
)

var (
	serialPort = flag.String("serial-port", "/dev/ttyUSB0", "path to the serial port")
	newID      = flag.Int("id", 0, "ID to set to the servo")
	oldID      = flag.Int("current-id", 1, "Current ID of servo")
	get        = flag.Bool("get", false, "do a get instead of a set to find out what id the servo has that is connected")
)

func main() {
	flag.Parse()

	if !*get {
		if *newID < 1 || *newID > 253 {
			fmt.Println("Invalid new ID min 1, max 253")
			return
		}

		if *oldID < 1 || *oldID > 253 {
			fmt.Println("Invalid current ID min 1, max 253")
			return
		}
	}

	sOpts := serial.OpenOptions{
		PortName:              *serialPort,
		BaudRate:              115200,
		DataBits:              8,
		StopBits:              1,
		MinimumReadSize:       0,
		InterCharacterTimeout: 100,
	}

	fmt.Println("opening serial")
	srl, err := serial.Open(sOpts)
	if err != nil {
		log.Fatalf("error opening serial port: %s\n", err)
	}
	defer srl.Close()
	var b []byte
	log.Info("purging serial buffer")
	b, err = ioutil.ReadAll(srl)
	if err != nil {
		log.Fatalf("error purging serial buffer: %s\n", err)
	}
	log.Infof("purged %d bytes", len(b))

	network := lx16a.NewNetwork(srl)
	servo := lx16a.NewServo(network, uint8(*oldID))

	if *get {
		id, err := servo.GetID()
		if err != nil {
			fmt.Println("Failed to get ID: ", err)
			return
		}

		fmt.Println("Servo ID: ", id)
		return
	}

	if err = servo.SetID(uint8(*newID)); err != nil {
		fmt.Println("setting id failed")
	}
	fmt.Println("ID successfully set: ", *newID)
}
