module gitlab.com/gamerscomplete/robotics/servo-controller

go 1.13

require (
	periph.io/x/conn/v3 v3.6.7
	periph.io/x/devices/v3 v3.6.9
	periph.io/x/host/v3 v3.6.7
)
