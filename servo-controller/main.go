package main

import (
	"log"
	"fmt"
	"time"

	"periph.io/x/conn/v3/i2c/i2creg"
	"periph.io/x/conn/v3/physic"
	"periph.io/x/conn/v3/gpio"
	"periph.io/x/devices/v3/pca9685"
	"periph.io/x/host/v3"
)

type Config struct {
    frequency physic.Frequency
    minDuty gpio.Duty
    maxDuty gpio.Duty
    minAngle physic.Angle
    maxAngle physic.Angle
}

var AnalogConfig = Config{
    frequency: 50,
	//minDuty of 200 required for mg91 servos
    //minDuty: 150,
    minDuty: 200,
    maxDuty: 470,
    minAngle: 0,
    maxAngle: 180,
}

var DigitalConfig = Config{
    frequency: 300,
    minDuty: 650,
    maxDuty: 4550,
    minAngle: 0,
    maxAngle: 180,
}

func main() {
	config := AnalogConfig
//	config := DigitalConfig

	fmt.Println("starting")
	_, err := host.Init()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("opening i2c")
	bus, err := i2creg.Open("")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("newi2c")
	pca, err := pca9685.NewI2C(bus, pca9685.I2CAddr)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("setting pwm")
	if err := pca.SetPwmFreq(config.frequency * physic.Hertz); err != nil {
		log.Fatal(err)
	}

	fmt.Println("setting all pwm")
	if err := pca.SetAllPwm(0, 0); err != nil {
		log.Fatal(err)
	}
	fmt.Println("new servo group")
	servos := pca9685.NewServoGroup(pca, config.minDuty, config.maxDuty, config.minAngle, config.maxAngle)

	for {
		fmt.Println("setting angle to 0")
		for i := 0; i < 15; i++ {
			servos.GetServo(i).SetAngle(0)
		}
		time.Sleep(2*time.Second)
		fmt.Println("setting angle to 180")
		for i := 0; i < 15; i++ {
			servos.GetServo(i).SetAngle(180)
		}
		time.Sleep(2*time.Second)
	}
}
