package lx16a

import ()

type Command int

const (
	/*
		Length: 7
		Parameter 1: lower 8 bits of angle value
		Parameter 2: higher 8 bits of angle value.range 0~1000. corresponding to the
			servo angle of 0 ~ 240 °, that means the minimum angle of the servo can be
			varied is 0.24 degree.
		Parameter 3: lower 8 bits of time value
		Parameter 4: higher 8 bits of time value. the range of time is 0~30000ms.
			When the command is sent to servo, the servo will be rotated from current
			angle to parameter angle at uniform speed within parameter time. After the
			command reaches servo, servo will rotate immediately.
	*/
	SERVO_MOVE_TIME_WRITE Command = 1
	/*
		Length: 3
		Read the angle and time value which sent by SERVO_MOVE_TIME_WRITE to the servo
	*/
	SERVO_MOVE_TIME_READ Command = 2
	/*
		Length: 7
		Parameter1: lower 8 bits of preset angle
		Parameter2: higher 8 bits of preset angle. range 0~1000. corresponding to the
			servo angle of 0 ~ 240 °. that means the minimum angle of the servo can be
			varied is 0.24 degree.
		Parameter3: lower 8 bits of preset time
		Parameter3: higher 8 bits of preset time. the range of time is 0~30000ms.
			The function of this command is similar to this
			“SERVO_MOVE_TIME_WRITE” command in the first point. But the difference
			is that the servo will not immediately turn when the command arrives at the
			servo,the servo will be rotated from current angle to parameter angle at uniform
			speed within parameter time until the command name SERVO_MOVE_START sent
			to servo(command value of 11), then the servo will be rotated from current
			ngle to setting angle at uniform speed within setting time
	*/
	SERVO_MOVE_TIME_WAIT_WRITE Command = 7
	/*
		Length: 3
		Read the preset angle and preset time value which sent by
		SERVO_MOVE_TIME_WAIT_WRITE to the serv
	*/
	SERVO_MOVE_TIME_WAIT_READ Command = 8
	/*
		Length: 3
		With the use of command SERVO_MOVE_TIME_WAIT_WRITE,
	*/
	SERVO_MOVE_START Command = 11
	/*
		Length: 3
		When the command arrives at the servo, it will stop running immediately if the
		servo is rotating, and stop at the current angle position.
	*/
	SERVO_MOVE_STOP Command = 12
	/*
	   Length: 4
	   Parameter 1: The servo ID, range 0 ~ 253, defaults to 1. The command will
	   re-write the ID value to the servo and save it even when power-down.
	*/
	SERVO_ID_WRITE Command = 13
	/*
	   Length: 3
	   Read servo ID
	*/
	SERVO_ID_READ Command = 14
	/*
	   Length: 4
	   Parameter 1: servo deviation, range -125~ 125, The corresponding angle of
	   -30 ° ~ 30 °, when this command reach to the servo, the servo will immediately
	   rotate to adjust the deviation.
	   Note 1:The adjusted deviation value is not saved when power-down by this
	   command, if you want to save please refer to point 10.
	   Note 2: Because the parameter is “signed char” type of data, and the
	   command packets to be sent are “unsigned char” type of data, so before
	   sending, parameters are forcibly converted to “unsigned char” data and then
	   put them in command packet.
	*/
	SERVO_ANGLE_OFFSET_ADJUST Command = 17
	/*
	   Length: 3
	   Save the deviation value, and support “power-down save”. The adjustment of
	   the deviation is stated in point 9
	*/
	SERVO_ANGLE_OFFSET_WRITE Command = 18
	/*
	   Length: 7
	   Read the deviation value set by the servo,
	*/
	SERVO_ANGLE_OFFSET_READ Command = 19
	/*
	   Length: 7
	   Parameter 1: lower 8 bits of minimum angleParameter 2: higher 8 bits of minimum angle, range 0~1000
	   Parameter 3: lower 8 bits of maximum angle
	   Parameter 4: higher 8 bits of maximum angle, range 0~1000
	   And the minimum angle value should always be less than the maximum angle
	   value. The command is sent to the servo, and the rotation angle of the servo
	   will be limited between the minimum and maximum angle. And the angle limit
	   value supports ‘power-down save’.
	*/
	SERVO_ANGLE_LIMIT_WRITE Command = 20
	/*
	   Length: 3
	   Read the angle limit value of the servo
	*/
	SERVO_ANGLE_LIMIT_READ Command = 21
	/*
	   Length: 7
	   Parameter 1: lower 8 bits of minimum input voltage
	   Parameter 2: higher 8 bits of minimum input voltage, range 4500~12000mv
	   Parameter 3: lower 8 bits of maximum input voltage
	   Parameter 4: higher 8 bits of maximum input voltage, range 4500~12000mv
	   And the minimum input voltage should always be less than the maximum input
	   voltage. The command is sent to the servo, and the input voltage of the servo
	   will be limited between the minimum and the maximum. If the servo is out of
	   range, the led will flash and alarm (if an LED alarm is set). In order to protect
	   the servo, the motor will be in the unloaded power situation, and the servo will
	   not output torque and the input limited voltage value supports for power-down
	   save.
	*/
	SERVO_VIN_LIMIT_WRITE Command = 22
	/*
	   Length: 3
	   Read the angle limit value of the servo, for the details of the instruction packet
	   that the servo returns to host computer
	*/
	SERVO_VIN_LIMIT_READ Command = 23
	/*
	   Length: 4
	   Parameter 1: The maximum temperature limit inside the servo range
	   50~100°C, the default value is 85°C, if the internal temperature of the servo
	   exceeds this value the led will flash and alarm (if an LED alarm is set). In order
	   to protect the servo, the motor will be in the unloaded power situation, and the
	   servo will not output torque until the temperature below this value of the servo,
	   then it will once again enter the working state.and this value supports for
	   power-down save.
	*/
	SERVO_TEMP_MAX_LIMIT_WRITE Command = 24
	/*
	   Length: 3
	   Read the maximum temperature limit value inside the servo
	*/
	SERVO_TEMP_MAX_LIMIT_READ Command = 25
	/*
	   Length: 3
	   Read the Real-time temperature inside the servo
	*/
	SERVO_TEMP_READ Command = 26
	/*
	   Length: 3
	   Read the current input voltage inside the servo
	*/
	SERVO_VIN_READ Command = 27
	/*
	   Length: 3
	   Read the current angle value of the servo
	*/
	SERVO_POS_READ Command = 28
	/*
	   Length: 7
	   Parameter 1: Servo mode, range 0 or 1, 0 for position control mode, 1 for
	   motor control mode, default 0,
	   Parameter 2: null value
	   Parameter 3: lower 8 bits of rotation speed value
	   Parameter 4: higher 8 bits of rotation speed value. range -1000~1000,
	   Only in the motor control mode is valid, control the motor speed, the value of
	   the negative value represents the reverse, positive value represents the
	   forward rotation. Write mode and speed do not support power-down save.
	   Note: Since the rotation speed is the “signed short int” type of data, it is forced
	   to convert the data to “unsigned short int “type of data before sending the
	   command packet.
	*/
	SERVO_OR_MOTOR_MODE_WRITE Command = 29
	/*
	   Length: 3
	   Read the relative values of the servo
	*/
	SERVO_OR_MOTOR_MODE_READ Command = 30
	/*
	   Length: 4
	   Parameter 1: Whether the internal motor of the servo is unloaded power-down
	   or not, the range 0 or 1, 0 represents the unloading power down, and the servo
	   has no torque output. 1 represents the loaded motor, then the servo has a
	   torque output, the default value is 0.
	*/
	SERVO_LOAD_OR_UNLOAD_WRITE Command = 31
	/*
	   Length: 3
	   Read the state of the internal motor of the servo.
	*/
	SERVO_LOAD_OR_UNLOAD_READ Command = 32
	/*
	   Length: 4
	   Parameter 1: LED light/off state, the range 0 or 1, 0 represents that the LED is
	   always on. 1 represents the LED off, the default 0, and support power-down
	   save
	*/
	SERVO_LED_CTRL_WRITE Command = 33
	/*
	   Length: 3
	   Read the state of the LED light.
	*/
	SERVO_LED_CTRL_READ Command = 33
	/*
	   Length: 4
	   Parameter 1: what faults will cause LED flashing alarm value, range 0~7
	   There are three types of faults that cause the LED to flash and alarm,
	   regardless of whether the LED is in or off. The first fault is that internal
	   temperature of the servo exceeds the maximum temperature limit (this value is
	   set at point 16). The second fault is that the servo input voltage exceeds the
	   limit value (this value is set at 14 points). The third one is
	   when locked-rotor occurred.

	   This value corresponds to the fault alarm relationship as shown below:
	   |---|---------------------------------------------|
	   | 0 | No alarm                                    |
	   | 1 | Over temperature                            |
	   | 2 | Over voltage                                |
	   | 3 | Over temperature and over voltage           |
	   | 4 | Locked-rotor                                |
	   | 5 | Over temperature and stalled                |
	   | 6 | Over voltage and stalled                    |
	   | 7 | Over temperature, over voltage, and stalled |
	   |---|---------------------------------------------|
	*/
	SERVO_LED_ERROR_WRITE Command = 35
	/*
	   Length: 3
	   Read the servo fault alarm value.
	*/
	SERVO_LED_ERROR_READ Command = 36
)
