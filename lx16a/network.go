package lx16a

import (
	"bytes"
	"fmt"
	"io"
	"time"
	"sync"
)

const (
	// Send an instruction to all servos
	BroadcastIdent byte = 0xFE // 254
)

type Network struct {
	Serial io.ReadWriteCloser
	// The time to wait for a read to complete before giving up
	Timeout time.Duration
	sync.Mutex
}

func NewNetwork(serial io.ReadWriteCloser) *Network {
	return &Network{
		Serial:  serial,
		Timeout: 10 * time.Millisecond,
	}
}

func (network *Network) readFixed(payload []byte) (n int, err error) {
	start := time.Now()
	retry := 1 * time.Millisecond

	for n < len(payload) {
		m, err := network.Serial.Read(payload[n:])
		n += m

		if err != nil && err != io.EOF {
			return m, err
		}

		// If past the timeout, abort
		if time.Since(start) >= network.Timeout {
			return n, fmt.Errorf("read timed out")
		}

		// If no bytes were read, back off exponentially. This is just to avoid
		// flooding the network with retries if a servo isn't responding.
		if m == 0 {
			time.Sleep(retry)
			retry *= 2
		}
	}

	return n, nil
}

func (network *Network) Read(expectedID uint8) ([]byte, error) {
	//read the first 5 bytes which is up to the command in the packet structure
	header := make([]byte, 5)
	n, err := network.readFixed(header)
	if err != nil {
		return nil, fmt.Errorf("failed to read packet header: %s", err)
	}
	if n != 5 {
		return nil, fmt.Errorf("failed to read packet header: expected %d bytes, got %d", 5, n)
	}

	//make sure the first 2 bytes are 0x55 per the spec
	if header[0] != 0x55 || header[1] != 0x55 {
		fmt.Println("header: ", header)
		return nil, fmt.Errorf("bad packet header 0x%02X 0x%02X", header[0], header[1])
	}

	actualID := header[2]
	length := header[3]
	payloadLength := length - 3

	network.Lock()
	defer network.Unlock()

	var packetBuf []byte
	if payloadLength > 0 {
		packetBuf = make([]byte, payloadLength)
		_, err := network.readFixed(packetBuf)
		if err != nil {
			return nil, fmt.Errorf("reading %d params: %s", payloadLength, err)
		}
	}

	checksum := make([]byte, 1)
	_, err = network.readFixed(checksum)
	if err != nil {
		return nil, fmt.Errorf("failed reading checksum: %s", err)
	}

	if uint8(actualID) != expectedID {
		return packetBuf, fmt.Errorf("got unexpected servo ID. expected: %v, got %v", expectedID, actualID)
	}
	return packetBuf, nil
}

func (network *Network) Write(payload []byte) (int, error) {
	network.Lock()
	defer network.Unlock()
	return network.Serial.Write(payload)
}

func CreatePacket(id uint8, command Command, parameters []byte) []byte {
	//header, id, data length, command, parameters, checksum

	length := uint8(len(parameters) + 3) //length of all the parameters + 1 for command, +1 for id, +1 for length = 3
	buf := new(bytes.Buffer)
	buf.Write([]byte{
		0x55, //header
		0x55, //header
		id,
		length,
		byte(command),
	})
	buf.Write(parameters)
	buf.WriteByte(checksum(id, length, uint8(command), parameters))
	return buf.Bytes()
}

/*
	Checksum=~(ID+ Length+Cmd+ Prm1+...PrmN)If the numbers in the
	brackets are calculated and exceeded 255,Then take the lowest one byte, "~"
	means Negation.
*/
func checksum(id uint8, length uint8, cmd uint8, parameters []uint8) uint8 {
	var paramsSummed uint8
	for i := 0; i < len(parameters); i++ {
		paramsSummed += parameters[i]
	}
	var checksum int
	checksum = 255 - (int((id + length + cmd + paramsSummed)) % 256)
	if checksum > 255 {
		fmt.Println("checksum calculation failed, value too large: ", checksum)
	}
	return uint8(checksum)
}

func (network *Network) Flush() {
	buf := make([]byte, 128)
	var n int

	for {
		n, _ = network.Serial.Read(buf)
		fmt.Printf(".. %v\n", buf)
		if n == 0 {
			break
		}
	}
}
