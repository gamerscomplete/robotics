package main

import (
	"fmt"
	"log"
	"time"

	"periph.io/x/conn/v3/analog"
	"periph.io/x/conn/v3/gpio"
	"periph.io/x/conn/v3/i2c/i2creg"
	"periph.io/x/conn/v3/physic"
	"periph.io/x/devices/v3/ads1x15"
	"periph.io/x/devices/v3/pca9685"
	"periph.io/x/host/v3"
)

type ServoConfig struct {
	frequency physic.Frequency
	minDuty   gpio.Duty
	maxDuty   gpio.Duty
	minAngle  physic.Angle
	maxAngle  physic.Angle
}

var AnalogConfig = ServoConfig{
	frequency: 50,
	//minDuty of 200 required for mg91 servos
	//minDuty: 150,
	minDuty:  200,
	maxDuty:  470,
	minAngle: 0,
	maxAngle: 180,
}

var DigitalConfig = ServoConfig{
	frequency: 300,
	minDuty:   650,
	maxDuty:   4550,
	minAngle:  0,
	maxAngle:  180,
}

func main() {
    config := AnalogConfig

	_, err := host.Init()
	if err != nil {
		log.Fatal(err)
	}

	bus, err := i2creg.Open("")
	if err != nil {
		log.Fatal(err)
	}

	//Setup pca9685
	pca, err := pca9685.NewI2C(bus, pca9685.I2CAddr)
	if err != nil {
		log.Fatal(err)
	}
	if err := pca.SetPwmFreq(config.frequency * physic.Hertz); err != nil {
		log.Fatal(err)
	}
	servos := pca9685.NewServoGroup(pca, config.minDuty, config.maxDuty, config.minAngle, config.maxAngle)

	//setup adc
	adc, err := ads1x15.NewADS1115(bus, &ads1x15.DefaultOpts)
	if err != nil {
		log.Fatalln(err)
	}

	joystickXAxis, err := adc.PinForChannel(ads1x15.Channel0, 5*physic.Volt, 1*physic.Hertz, ads1x15.SaveEnergy)
	if err != nil {
		log.Fatalln(err)
	}
	defer joystickXAxis.Halt()

/*
	joystickXAxis, err := adc.PinForChannel(ads1x15.Channel1, 5*physic.Volt, 1*physic.Hertz, ads1x15.SaveEnergy)
	if err != nil {
		log.Fatalln(err)
	}
	defer joystickYAxis.Halt()


	currentSensor1, err := adc.PinForChannel(ads1x15.Channel2, 5*physic.Volt, 1*physic.Hertz, ads1x15.SaveEnergy)
	if err != nil {
		log.Fatalln(err)
	}
	defer currentSensor2.Halt()

	currentSensor2, err := adc.PinForChannel(ads1x15.Channel3, 5*physic.Volt, 1*physic.Hertz, ads1x15.SaveEnergy)
	if err != nil {
		log.Fatalln(err)
	}
	defer currentSensor2.Halt()
*/


	fmt.Println("starting analog reads")



/*	min 10
	max 26500
	idle 13585
*/
	var lastReading analog.Sample
	for {
		reading, err := joystickXAxis.Read()
		if err != nil {
			log.Fatalln(err)
		}
		if reading != lastReading {
			fmt.Println("reading:", reading)
			lastReading = lastReading
			//TODO: make this actually do something. putting dummy code here so more doesnt have to be commented out
			angle := mapValue(int(reading.Raw), 10, 26500, 0, 180)
			fmt.Println("Setting angle to:", angle)
			servos.GetServo(0).SetAngle(physic.Angle(angle))
		}
		time.Sleep(200 * time.Millisecond)
	}




	//setup pins for joystick button
	//setup stepper controller

	//link joystick left/right to servo
	//link joystick up/down to stepper

	//read current sensor on 12v and 5v
	//read voltage sensor on 12v

	//TODO: support nordic wireless unit
	//TODO: setup second adc, need to figure out how to change the i2c address
	//TODO: setup the light strip
	//TODO: implement digital temperature sensor
	//TODO: implement analog temperature sensor
}

func mapValue(x, inMin, inMax, outMin, outMax int) int {
    return (x-inMin)*(outMax-outMin)/(inMax-inMin) + outMin
}
